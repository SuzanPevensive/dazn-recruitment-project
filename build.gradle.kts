buildscript {

    val kotlinVersion = "1.4.30"
    val hiltVersion = "2.33-beta"

    extra["kotlinVersion"] = kotlinVersion
    extra["workManagerVersion"] = "2.4.0"

    extra["hiltVersion"] = hiltVersion
    extra["hiltCompilerVersion"] = "1.0.0-beta01"

    extra["roomVersion"] = "2.3.0-beta03"
    extra["retrofitVersion"] = "2.9.0"

    repositories {
        google()
        jcenter()
        maven("https://plugins.gradle.org/m2/")
    }
    dependencies {
        classpath("com.android.tools.build:gradle:4.1.2")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion")
        classpath("com.google.dagger:hilt-android-gradle-plugin:$hiltVersion")
        classpath("org.jlleitschuh.gradle:ktlint-gradle:7.1.0")
        classpath("de.mannodermaus.gradle.plugins:android-junit5:1.6.2.0")
    }
}

allprojects {
    tasks.withType<JavaCompile> {
        options.compilerArgs.add("-Xlint:deprecation")
    }
    repositories {
        google()
        jcenter()
    }
    apply(plugin = "org.jlleitschuh.gradle.ktlint")
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}
