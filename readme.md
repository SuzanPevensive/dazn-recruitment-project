# Dazn - recruitment project

### App writed by Zuzanna Kowaliszyn as recruitment project for Dazn Company

**App contains 2 fragments (pages) and 1 activity with ViewPager2 operated by BottomNavigation**

First page contains list (`RecyclerView`) of events from backend, sorted by date
Data of this list is all past events available from request, so i use additional `chunking` and list recharging, list have loading animation too
The list data is download on app start and save to app storage, then `RecyclerView` loading next chunks from storage
Page have `swipe refresh view`, which makes it possible again download data and refreshing list
After tap on list item, show dialog with player and start playing target video
***
Second page contains list (`RecyclerView`) of schedule (events list) from backend, sorted by date
Data of this list is only future events, so i use only classic `RecyclerView` without chunking, `RecyclerView` has `ViewHolder` management mechanism and can dynamic generate new content
The list data is downloading on page load and at every 30 seconds
Page have `swipe refresh view`, which makes it possible again download data and refreshing list
List `ViewHolders` are bonded to main lifecycleOwner, so change data after every download doesn't make the entire list blink and the scroll position is always security
***
Player is the simple widget build on `ExoPlayer2` library
The item date of both list is formatted by `PrettyTime` library, and I think is similar to the example from pdf, or maybe better

**Project have unit test for all classes, which not required instrumendet to operate**

Tests were based on Mockito and some Junit extensions

**App written in Kotlin using AndroidX and Ktx libraries.**

App is built in `MVVM` model with `Hilt Dependency Injection`, communication between logical
layers is done with `custom Livedata extensions`. Layouts use `DataBinding` to communicate
with `ViewModels`.
Connection with server is supported by `Retrofit2` with `Gson` and `Scalars` converters.
Local storage is operated by `Room`.
Data transport is provided by `Coroutines`.

Project following the rules `SOLID` and `Clean Code` most of injected classes are only implementations of their interfaces.

Names of project classes is creating with `Java naming convention` and `Ktlint rules`, but resources names and ids are write with `snake_case format`.

**The first part of the name is the type of data to be included, or name of data parent, for example:**

- PATH_GET_EVENTS          -   url path to get events
- @+id/list_item_subtitle  -   subtitle of event list item

**But in contrast names of classes and variables always begin with their describe, for example:**

- ErrorManager   -    Singleton class used to detection and management App errors
- dataManager    -    variable includes injection of DataManager

**However the names of methods always begin with action type, for example:**

- ResponseConverter::**convertEventResponse**   -   method to convert events `response dto` to `response data object`
- DataManager::**clearAllData**                 -   method to clear app storage

Each default `it` argument is named according to its contents. Exceptions are "Name shadowed" situations, observers, and sometimes Collection Api functions

##### Project structure:
- const         -     All **const** variable in project
- enums         -     All **enum** classes in project
- converter     -     Singletons to convert `dto` object to data object
- hilt          -     All `Hilt` modules and components
- manager       -     Singletons to manage all app background processes ( network connection, media provider, permissions etc. )
- model         -     Includes Requests bodies, Responses `dto`, Data objects, `Room entities`
- network       -     Directory for all endpoints and potential `Retrofit2 interceptors`
- ui            -     All files related to `Activities, Fragment and Dialogs`, includes too `adapters` and `layoutManagers` for `RecyclerView` and there are other potential classes used  to build `UI and UX`
- utils         -     All helper classes and extensions
  - dataBindingAdapter - All `DataBinding adapters` like event handlers or transport interface of complex data such as `Drawable` between `Layouts` and `ViewModels`
  - event       -     Events helper classes includes additional information and actions related with some user events, for example: `SingleClickEvent` created and send to handler by `onSingleClick DataBinding extension adapter` when user click target element. Until event object invoke **done** method user can't click again the element.
  - extension   -     Kotlin extensions of system classes
  - liveData    -     Kotlin extensions of `LiveData` interface, for example `ScopedLiveData`, which detects **current thread** and automatically run **setValue** or **postValue**, or other **SingleEventLiveData** used to single notification about some action.
  - view        -     Custom Views
- viewModel     -     All `ViewModels`, their `Repositories` and classes related to them
- worker        -     All **workers** in project
