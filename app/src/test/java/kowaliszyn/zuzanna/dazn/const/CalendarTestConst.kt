package kowaliszyn.zuzanna.dazn.const

import java.text.SimpleDateFormat

object CalendarTestConst {

    val DEFAULT_FORMAT =
        SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", LocaleConst.DEFAULT_LOCALE)
}
