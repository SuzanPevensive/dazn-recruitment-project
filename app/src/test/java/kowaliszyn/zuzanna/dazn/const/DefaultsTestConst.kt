package kowaliszyn.zuzanna.dazn.const

import java.util.*

object DefaultsTestConst {

    const val STRING = ""
    const val BOOLEAN = false
    const val INTEGER = -1
    const val FLOAT = INTEGER.toFloat()
    const val LONG = INTEGER.toLong()
    const val DOUBLE = INTEGER.toDouble()
    val DATE = Date(LONG)
}
