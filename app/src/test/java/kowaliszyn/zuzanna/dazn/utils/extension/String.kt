package kowaliszyn.zuzanna.dazn.utils.extension

import java.util.Random

fun String.Companion.random(length: Int): String {
    val generator = Random()
    var randomString = ""
    repeat(length) {
        randomString += (generator.nextInt(96) + 32).toChar()
    }
    return randomString
}
