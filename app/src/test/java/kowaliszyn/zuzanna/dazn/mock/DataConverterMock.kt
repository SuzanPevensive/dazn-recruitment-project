package kowaliszyn.zuzanna.dazn.mock

import kowaliszyn.zuzanna.dazn.converter.DataConverter

object DataConverterMock {
    fun getInstance() = DataConverter()
}
