package kowaliszyn.zuzanna.dazn.mock

import com.nhaarman.mockitokotlin2.spy
import kowaliszyn.zuzanna.dazn.manager.impl.ErrorManagerImpl

object ErrorManagerMock {

    fun getInstance(): ErrorManagerImpl = spy()
}
