package kowaliszyn.zuzanna.dazn.mock

import kowaliszyn.zuzanna.dazn.converter.RoomConverter

object RoomConverterMock {
    fun getInstance() = RoomConverter()
}
