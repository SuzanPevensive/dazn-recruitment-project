package kowaliszyn.zuzanna.dazn.mock

import kowaliszyn.zuzanna.dazn.converter.ResponseConverter

object ResponseConverterMock {
    fun getInstance() = ResponseConverter(DataConverterMock.getInstance())
}
