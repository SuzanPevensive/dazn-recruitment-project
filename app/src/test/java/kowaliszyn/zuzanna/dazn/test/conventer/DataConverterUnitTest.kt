package kowaliszyn.zuzanna.dazn.test.conventer

import kowaliszyn.zuzanna.dazn.const.*
import kowaliszyn.zuzanna.dazn.mock.DataConverterMock
import kowaliszyn.zuzanna.dazn.model.data.EventData
import kowaliszyn.zuzanna.dazn.model.response.converted.EventResponse
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class DataConverterUnitTest {

    private val dataConverter = DataConverterMock.getInstance()

    @Test
    fun convertEventsResponseTest() {

        val id = DefaultsTestConst.INTEGER
        val title = DefaultsTestConst.STRING
        val subtitle = DefaultsTestConst.STRING
        val date = DefaultsTestConst.DATE
        val imageUrl = DefaultsTestConst.STRING
        val videoUrl = DefaultsTestConst.STRING

        val eventDto = EventResponse(
            id,
            title,
            subtitle,
            date,
            imageUrl,
            videoUrl
        )
        val event = EventData(
            id,
            title,
            subtitle,
            date,
            imageUrl,
            videoUrl
        )
        dataConverter.apply {
            assertEquals(listOf(event), convertEventsResponse(listOf(eventDto)))
        }
    }
}
