package kowaliszyn.zuzanna.dazn.test.utils.liveData

import kowaliszyn.zuzanna.dazn.jUnitExtensions.InstantExecutorExtension
import kowaliszyn.zuzanna.dazn.utils.liveData.ClearableLiveData
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(InstantExecutorExtension::class)
class ClearableLiveDataUnitTest {

    private val clearableLiveData = ClearableLiveData<Int>()

    @Test
    fun clearableTest() {

        var someValue = 16
        clearableLiveData.observeForever {
            someValue -= 1
        }

        clearableLiveData.value = 12

        assertEquals(15, someValue)
        assertEquals(null, clearableLiveData.value)
    }
}
