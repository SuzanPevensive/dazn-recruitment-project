package kowaliszyn.zuzanna.dazn.test.manager

import java.lang.Exception
import kowaliszyn.zuzanna.dazn.jUnitExtensions.InstantExecutorExtension
import kowaliszyn.zuzanna.dazn.mock.ErrorManagerMock
import kowaliszyn.zuzanna.dazn.utils.exception.TestException
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(InstantExecutorExtension::class)
class ErrorManagerUnitTest {

    private val errorManager = ErrorManagerMock.getInstance()

    private val testException = TestException()
    private val secondTestException = TestException()
    private val someException = Exception("Some exception")

    @Test
    fun errorsManagementTest() {

        errorManager.apply {

            add(testException)
            add(secondTestException)
            add(someException)
            assertEquals(someException, lastError.value)
            assertEquals(secondTestException, get(TestException::class.java))
            assertEquals(testException, get(TestException::class.java, 0))
            assertEquals(someException, get(Exception::class.java))
            assertEquals(secondTestException, get(1))
            assertEquals(someException, get())
            assertEquals(listOf(testException, secondTestException, someException), getAll())
            assertEquals(
                listOf(testException, secondTestException),
                getAll(TestException::class.java)
            )

            remove(Exception::class.java)
            assertEquals(null, get(Exception::class.java))
            remove(TestException::class.java, 1)
            assertEquals(testException, get(TestException::class.java))
            clear()
            assertEquals(null, get())
        }
    }
}
