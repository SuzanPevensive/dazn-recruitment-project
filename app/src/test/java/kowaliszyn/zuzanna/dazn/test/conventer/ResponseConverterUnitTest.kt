package kowaliszyn.zuzanna.dazn.test.conventer

import kowaliszyn.zuzanna.dazn.const.*
import kowaliszyn.zuzanna.dazn.mock.ResponseConverterMock
import kowaliszyn.zuzanna.dazn.model.response.converted.EventResponse
import kowaliszyn.zuzanna.dazn.model.response.dto.EventResponseDto
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ResponseConverterUnitTest {

    private val responseConverter = ResponseConverterMock.getInstance()

    @Test
    fun convertEventsResponseTest() {

        val id = DefaultsTestConst.INTEGER
        val title = DefaultsTestConst.STRING
        val subtitle: String? = null
        val date = DefaultsTestConst.DATE
        val imageUrl = DefaultsTestConst.STRING
        val videoUrl: String? = null

        val eventDto = EventResponseDto(
            id,
            title,
            subtitle,
            CalendarTestConst.DEFAULT_FORMAT.format(date),
            imageUrl,
            videoUrl
        )
        val event = EventResponse(
            id,
            title,
            DefaultsTestConst.STRING,
            date,
            imageUrl,
            DefaultsTestConst.STRING
        )
        responseConverter.apply {
            assertEquals(listOf(event), convertEventsResponse(listOf(eventDto)))
        }
    }
}
