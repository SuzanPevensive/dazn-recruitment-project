package kowaliszyn.zuzanna.dazn.test.utils.liveData

import kowaliszyn.zuzanna.dazn.jUnitExtensions.InstantExecutorExtension
import kowaliszyn.zuzanna.dazn.utils.liveData.EventLiveData
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(InstantExecutorExtension::class)
class EventLiveDataUnitTest {

    private val eventLiveData = EventLiveData()

    @Test
    fun runEventTest() {

        var someValue = 17
        eventLiveData.observeForever {
            someValue -= 1
        }

        eventLiveData.run()
        eventLiveData.run()

        assertEquals(15, someValue)
    }
}
