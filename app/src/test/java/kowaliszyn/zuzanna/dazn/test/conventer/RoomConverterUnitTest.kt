package kowaliszyn.zuzanna.dazn.test.conventer

import kowaliszyn.zuzanna.dazn.mock.RoomConverterMock
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.*

class RoomConverterUnitTest {

    private val roomConverter = RoomConverterMock.getInstance()

    @Test
    fun convertDateToLongTest() {
        assertEquals(10, roomConverter.toDate(10).time)
    }

    @Test
    fun convertLongToDateTest() {
        assertEquals(10, roomConverter.fromDate(Date(10)))
    }
}
