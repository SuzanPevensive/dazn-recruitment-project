package kowaliszyn.zuzanna.dazn.const

object FragmentConst {

    const val ARGUMENT_MODE = "MODE"

    const val RELOAD_SCHEDULER_TIMER_DELAY = 30 * 1000L
}
