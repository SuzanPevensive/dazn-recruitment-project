package kowaliszyn.zuzanna.dazn.const

object PathsConst {
    const val PATH_GET_EVENTS = "getEvents"
    const val PATH_GET_SCHEDULE = "getSchedule"
}
