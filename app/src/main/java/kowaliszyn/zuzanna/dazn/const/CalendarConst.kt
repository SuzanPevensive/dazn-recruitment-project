package kowaliszyn.zuzanna.dazn.const

import org.ocpsoft.prettytime.PrettyTime
import java.text.SimpleDateFormat
import java.util.Calendar

object CalendarConst {
    val DEFAULT_CALENDAR: Calendar get() = Calendar.getInstance(LocaleConst.DEFAULT_LOCALE)
    val DATETIME_EVENT_FORMAT = PrettyTime()
    val DATETIME_FORMAT =
        SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", LocaleConst.DEFAULT_LOCALE)
    val DATETIME_T_FORMAT =
        SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", LocaleConst.DEFAULT_LOCALE)
    val DATETIME_Z_FORMAT =
        SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", LocaleConst.DEFAULT_LOCALE)
}
