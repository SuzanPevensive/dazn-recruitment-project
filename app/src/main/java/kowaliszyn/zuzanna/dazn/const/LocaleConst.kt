package kowaliszyn.zuzanna.dazn.const

import java.util.Locale

object LocaleConst {
    val DEFAULT_LOCALE: Locale get() = Locale.getDefault()
}
