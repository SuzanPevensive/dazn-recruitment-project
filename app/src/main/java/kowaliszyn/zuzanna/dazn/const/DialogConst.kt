package kowaliszyn.zuzanna.dazn.const

object DialogConst {
    const val TAG_DIALOG_VIDEO_PLAYER = "DIALOG_VIDEO_PLAYER"
    const val TAG_DIALOG_CONNECTION_MISSING = "DIALOG_CONNECTION_MISSING"
}
