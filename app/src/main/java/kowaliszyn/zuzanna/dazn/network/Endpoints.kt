package kowaliszyn.zuzanna.dazn.network

import kotlinx.coroutines.Deferred
import kowaliszyn.zuzanna.dazn.const.PathsConst
import kowaliszyn.zuzanna.dazn.model.response.dto.*
import retrofit2.http.*

interface Endpoints {

    @GET(PathsConst.PATH_GET_EVENTS)
    fun getEventsAsync(): Deferred<List<EventResponseDto?>?>

    @GET(PathsConst.PATH_GET_SCHEDULE)
    fun getScheduleAsync(): Deferred<List<EventResponseDto?>?>
}
