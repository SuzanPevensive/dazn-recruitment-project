package kowaliszyn.zuzanna.dazn.converter

import kowaliszyn.zuzanna.dazn.const.DefaultsConst.defIfNull
import kowaliszyn.zuzanna.dazn.converter.base.BaseConverter
import kowaliszyn.zuzanna.dazn.model.data.EventData
import kowaliszyn.zuzanna.dazn.model.response.converted.EventResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataConverter @Inject constructor() : BaseConverter() {

    private fun convertEventResponse(eventResponse: EventResponse) = eventResponse.run {
        EventData(
            defIfNull(id),
            defIfNull(title),
            defIfNull(subtitle),
            date,
            defIfNull(imageUrl),
            defIfNull(videoUrl),
        )
    }

    fun convertEventsResponse(eventsResponse: List<EventResponse>) =
        convertList(eventsResponse, ::convertEventResponse)
}
