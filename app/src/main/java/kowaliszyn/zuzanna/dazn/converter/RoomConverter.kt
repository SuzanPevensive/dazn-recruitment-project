package kowaliszyn.zuzanna.dazn.converter

import androidx.room.TypeConverter
import java.util.*

class RoomConverter {
    @TypeConverter
    fun toDate(value: Long?): Date {
        return Date(value ?: 0L)
    }
    @TypeConverter
    fun fromDate(date: Date?): Long {
        return date?.time ?: 0L
    }
}
