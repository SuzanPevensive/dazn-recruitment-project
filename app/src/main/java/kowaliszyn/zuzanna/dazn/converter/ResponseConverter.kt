package kowaliszyn.zuzanna.dazn.converter

import kowaliszyn.zuzanna.dazn.const.DefaultsConst.defIfNull
import kowaliszyn.zuzanna.dazn.converter.base.BaseConverter
import kowaliszyn.zuzanna.dazn.model.response.converted.*
import kowaliszyn.zuzanna.dazn.model.response.dto.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ResponseConverter @Inject constructor(val dataConverter: DataConverter) : BaseConverter() {

    private fun convertEventResponse(eventResponse: EventResponseDto?) = eventResponse.let {
        EventResponse(
            defIfNull(it?.id),
            defIfNull(it?.title),
            defIfNull(it?.subtitle),
            convertDate(it?.date),
            defIfNull(it?.imageUrl),
            defIfNull(it?.videoUrl),
        )
    }

    fun convertEventsResponse(eventsResponse: List<EventResponseDto?>) =
        convertList(eventsResponse, ::convertEventResponse)
}
