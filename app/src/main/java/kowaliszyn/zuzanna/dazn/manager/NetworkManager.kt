package kowaliszyn.zuzanna.dazn.manager

import kowaliszyn.zuzanna.dazn.model.response.converted.*

interface NetworkManager {

    suspend fun isConnected(): Boolean

    suspend fun getEvents(): List<EventResponse>?
    suspend fun getSchedule(): List<EventResponse>?
}
