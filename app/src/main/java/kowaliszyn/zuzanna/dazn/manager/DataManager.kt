package kowaliszyn.zuzanna.dazn.manager

import kowaliszyn.zuzanna.dazn.model.data.EventData

interface DataManager {

    suspend fun clearAllData()

    suspend fun getAllEvents(): List<EventData>
    suspend fun getEvents(chunk: Int): List<EventData>
    suspend fun getEvent(eventId: Int): EventData?
    suspend fun addEvents(events: List<EventData>)
    suspend fun addEvent(event: EventData)
    suspend fun editEvent(event: EventData)
    suspend fun removeEvent(eventId: Int)
    suspend fun removeAllEvents()
}
