package kowaliszyn.zuzanna.dazn.manager.impl

import android.content.Context
import androidx.room.Room
import dagger.hilt.android.qualifiers.ApplicationContext
import kowaliszyn.zuzanna.dazn.const.ChunkListsConst
import kowaliszyn.zuzanna.dazn.manager.DataManager
import kowaliszyn.zuzanna.dazn.model.MainDatabase
import kowaliszyn.zuzanna.dazn.model.data.EventData
import java.util.*
import javax.inject.Inject

class DataManagerImpl @Inject constructor(
    @ApplicationContext private val context: Context
) : DataManager {

    private val mainDatabase = Room.databaseBuilder(
        context,
        MainDatabase::class.java,
        "database_main"
    ).run {
        fallbackToDestructiveMigration()
        build()
    }

    private val eventsBase = mainDatabase.eventsEntityDao()

    override suspend fun clearAllData() {
        eventsBase.removeAllEvents()
    }

    override suspend fun getAllEvents() = eventsBase.getAllEvents()

    override suspend fun getEvents(chunk: Int) =
        eventsBase.getSomeEvents(
            ChunkListsConst.LIST_CHUNK_SIZE * chunk,
            ChunkListsConst.LIST_CHUNK_SIZE * (chunk + 1)
        )

    override suspend fun getEvent(eventId: Int) = eventsBase.findEventById(eventId)

    override suspend fun addEvents(events: List<EventData>) =
        eventsBase.addEvents(*events.toTypedArray())

    override suspend fun addEvent(event: EventData) = eventsBase.addEvent(event)

    override suspend fun editEvent(event: EventData) = eventsBase.editEvent(event)

    override suspend fun removeEvent(eventId: Int) = eventsBase.removeEvent(
        EventData(eventId, "", "", Date(0L), "", "")
    )

    override suspend fun removeAllEvents() = eventsBase.removeAllEvents()
}
