package kowaliszyn.zuzanna.dazn.model.response.converted

import kowaliszyn.zuzanna.dazn.model.response.converted.base.BaseResponse
import java.util.*

data class EventResponse(
    var id: Int,
    var title: String,
    var subtitle: String,
    var date: Date,
    var imageUrl: String,
    var videoUrl: String,
) : BaseResponse
