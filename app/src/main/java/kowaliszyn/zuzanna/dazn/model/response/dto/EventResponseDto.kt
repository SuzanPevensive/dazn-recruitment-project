package kowaliszyn.zuzanna.dazn.model.response.dto

import kowaliszyn.zuzanna.dazn.model.response.dto.base.BaseResponseDto

data class EventResponseDto(
    var id: Int?,
    var title: String?,
    var subtitle: String?,
    var date: String?,
    var imageUrl: String?,
    var videoUrl: String?,
) : BaseResponseDto
