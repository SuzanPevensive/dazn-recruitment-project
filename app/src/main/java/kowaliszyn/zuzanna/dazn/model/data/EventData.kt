package kowaliszyn.zuzanna.dazn.model.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import kowaliszyn.zuzanna.dazn.model.data.base.BaseData
import java.util.*

@Entity(tableName = "Events")
data class EventData(
    @PrimaryKey var id: Int,
    var title: String,
    var subtitle: String,
    var date: Date,
    var imageUrl: String,
    var videoUrl: String,
) : BaseData
