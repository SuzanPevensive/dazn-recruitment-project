package kowaliszyn.zuzanna.dazn.model

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import kowaliszyn.zuzanna.dazn.converter.RoomConverter
import kowaliszyn.zuzanna.dazn.model.dao.EventDataDao
import kowaliszyn.zuzanna.dazn.model.data.EventData

@Database(entities = [EventData::class], version = 1, exportSchema = false)
@TypeConverters(RoomConverter::class)
abstract class MainDatabase : RoomDatabase() {
    abstract fun eventsEntityDao(): EventDataDao
}
