package kowaliszyn.zuzanna.dazn.model.dao

import androidx.room.*
import kowaliszyn.zuzanna.dazn.model.data.EventData

@Dao
interface EventDataDao {

    @Query("SELECT * FROM Events ORDER BY date ASC")
    suspend fun getAllEvents(): List<EventData>

    @Query("SELECT * FROM Events ORDER BY date LIMIT :count OFFSET :offset ")
    suspend fun getSomeEvents(offset: Int, count: Int): List<EventData>

    @Query("SELECT * FROM Events WHERE id LIKE :id LIMIT 1")
    suspend fun findEventById(id: Int): EventData

    @Insert
    suspend fun addEvent(event: EventData)

    @Insert
    suspend fun addEvents(vararg events: EventData)

    @Update
    suspend fun editEvent(vararg event: EventData)

    @Delete
    suspend fun removeEvent(event: EventData)

    @Query("DELETE FROM Events")
    fun removeAllEvents()
}
