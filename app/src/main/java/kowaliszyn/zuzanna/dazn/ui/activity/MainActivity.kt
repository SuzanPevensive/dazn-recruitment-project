package kowaliszyn.zuzanna.dazn.ui.activity

import android.os.Bundle
import androidx.activity.viewModels
import dagger.hilt.android.AndroidEntryPoint
import kowaliszyn.zuzanna.dazn.R
import kowaliszyn.zuzanna.dazn.manager.ErrorManager
import kowaliszyn.zuzanna.dazn.ui.activity.base.BaseActivity
import kowaliszyn.zuzanna.dazn.ui.adapter.MainActivityViewPagerAdapter
import kowaliszyn.zuzanna.dazn.viewModel.activity.MainActivityViewModel
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : BaseActivity<MainActivityViewModel>() {

    override val viewModel by viewModels<MainActivityViewModel>()

    @Inject
    override lateinit var errorManager: ErrorManager
    override val layoutId = R.layout.activity_main

    private val viewPagerAdapter = MainActivityViewPagerAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.initViewPager(viewPagerAdapter)
    }
}
