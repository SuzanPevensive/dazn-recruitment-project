package kowaliszyn.zuzanna.dazn.ui.activity.base

import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkInfo
import androidx.work.WorkManager
import kowaliszyn.zuzanna.dazn.const.DialogConst
import kowaliszyn.zuzanna.dazn.const.WorkConst
import kowaliszyn.zuzanna.dazn.enums.ConnectionState
import kowaliszyn.zuzanna.dazn.manager.ErrorManager
import kowaliszyn.zuzanna.dazn.ui.dialog.InfoBarConnectionMissingDialog
import kowaliszyn.zuzanna.dazn.utils.extension.uiTimer
import kowaliszyn.zuzanna.dazn.utils.liveData.UniqueLiveData
import kowaliszyn.zuzanna.dazn.viewModel.base.BaseViewModel
import kowaliszyn.zuzanna.dazn.worker.CheckConnectionWorker

abstract class BaseActivity<out VM : BaseViewModel<*, *>> : AppCompatActivity() {

    abstract val viewModel: VM

    protected abstract val errorManager: ErrorManager
    protected abstract val layoutId: Int

    protected open val checkConnectionState = true
    val connectionState = UniqueLiveData(ConnectionState.CONNECTED)

    private val workerManager by lazy { WorkManager.getInstance(applicationContext) }
    private val checkConnectionWorkRequest =
        OneTimeWorkRequestBuilder<CheckConnectionWorker>().build()

    private val connectionMissingDialog by lazy {
        InfoBarConnectionMissingDialog.newInstance {
            runCheckConnectionWorkRequest()
        }
    }

    private fun showConnectionMissingDialog() {
        if (!connectionMissingDialog.isVisible) {
            connectionMissingDialog.show(
                supportFragmentManager,
                DialogConst.TAG_DIALOG_CONNECTION_MISSING
            )
        }
    }

    private fun hideConnectionMissingDialog() {
        if (connectionMissingDialog.isVisible) {
            connectionMissingDialog.dismiss()
        }
    }

    private fun runCheckConnectionWorkRequest() {
        uiTimer(500) {
            workerManager.enqueueUniqueWork(
                WorkConst.WORK_NAME_CHECK_CONNECTION,
                ExistingWorkPolicy.REPLACE,
                checkConnectionWorkRequest
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.init(this, layoutId, LayoutInflater.from(this))
        setContentView(viewModel.rootView)

        setObservers()
        errorManager.handleErrors(this, viewModel::handleError)

        workerManager
            .getWorkInfoByIdLiveData(checkConnectionWorkRequest.id)
            .observe(
                this,
                { info ->
                    when {
                        info == null -> runCheckConnectionWorkRequest()
                        info.state == WorkInfo.State.SUCCEEDED -> {
                            connectionState.value = ConnectionState.CONNECTED
                            hideConnectionMissingDialog()
                            runCheckConnectionWorkRequest()
                        }
                        info.state == WorkInfo.State.FAILED -> {
                            connectionState.value = ConnectionState.NOT_CONNECTED
                            showConnectionMissingDialog()
                        }
                        info.state == WorkInfo.State.BLOCKED ||
                            info.state == WorkInfo.State.CANCELLED -> {
                            runCheckConnectionWorkRequest()
                        }
                    }
                }
            )
        if (checkConnectionState) runCheckConnectionWorkRequest()
    }

    open fun setObservers() {
    }

    override fun onBackPressed() {
    }
}
