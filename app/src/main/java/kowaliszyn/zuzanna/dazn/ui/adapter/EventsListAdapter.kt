package kowaliszyn.zuzanna.dazn.ui.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kowaliszyn.zuzanna.dazn.R
import kowaliszyn.zuzanna.dazn.const.CalendarConst
import kowaliszyn.zuzanna.dazn.databinding.ItemEventsListBinding
import kowaliszyn.zuzanna.dazn.model.data.EventData
import kowaliszyn.zuzanna.dazn.ui.adapter.base.ChoosableListAdapter
import kowaliszyn.zuzanna.dazn.utils.event.impl.SingleClickEvent
import kowaliszyn.zuzanna.dazn.utils.liveData.ScopedLiveData
import kowaliszyn.zuzanna.dazn.utils.liveData.UniqueLiveData
import java.lang.Exception

class EventsListAdapter constructor(
    list: List<EventData>,
    lifecycleOwner: LifecycleOwner? = null,
    private val chooseItemAction: (eventId: Int) -> Unit,
) : ChoosableListAdapter
<EventData, EventsListAdapter.EventsListViewHolderModel, ItemEventsListBinding>(
    list,
    lifecycleOwner,
    R.layout.item_events_list
) {

    fun setSchedule(list: List<EventData>) {
        if (list.size < this.list.size) {
            this.list.subList(list.size, this.list.size).forEachIndexed { index, event ->
                this.list.remove(event)
                queueItemRemoved(index)
            }
        }
        list.forEachIndexed { index, event ->
            if (index >= this.list.size) {
                this.list.add(event)
                queueItemInserted(index)
            } else {
                this.list[index] = event
                queueItemChanged(index)
            }
        }
    }

    override fun viewHolderModelBuilder(binding: ItemEventsListBinding) =
        EventsListViewHolderModel(binding, chooseItemAction)

    inner class EventsListViewHolderModel(
        binding: ItemEventsListBinding,
        chooseItemAction: (Int) -> Unit
    ) : ChoosableListAdapter.ChoosableViewHolderModel<EventData, ItemEventsListBinding>(
        binding,
        chooseItemAction
    ) {

        private val context: Context = binding.root.context

        val title = UniqueLiveData("")
        val subtitle = UniqueLiveData("")
        val date = UniqueLiveData("")
        val isWaiting = ScopedLiveData(false)

        private var backgroundColor = Color.TRANSPARENT
        private var imageUrl = ""

        val background = ScopedLiveData<Drawable>()

        override fun chooseItem(event: SingleClickEvent) {
            chooseItemAction(list[position].id)
            event.done()
        }

        override fun setData(data: EventData) {
            title.value = data.title
            subtitle.value = data.title
            date.value = CalendarConst.DATETIME_EVENT_FORMAT.format(data.date)
            (if (position % 2 == 0) Color.WHITE else Color.TRANSPARENT).let { backgroundColor ->
                if (this.backgroundColor != backgroundColor) {
                    this.backgroundColor = backgroundColor
                    background.value = ColorDrawable(backgroundColor)
                }
            }
            if (imageUrl != data.imageUrl) {
                imageUrl = data.imageUrl
                isWaiting.value = true
                Picasso.get().load(imageUrl).apply {
                    into(
                        binding.listItemImage,
                        object : Callback {
                            override fun onSuccess() {
                                isWaiting.value = false
                            }
                            override fun onError(e: Exception?) {
                                binding.listItemImage.background =
                                    ContextCompat.getDrawable(context, R.drawable.ic_logo)
                            }
                        }
                    )
                }
            }
        }
    }
}
