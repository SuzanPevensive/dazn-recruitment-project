package kowaliszyn.zuzanna.dazn.ui.adapter.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import kowaliszyn.zuzanna.dazn.const.ChunkListsConst

abstract class SimpleListAdapter
<T, VM : SimpleListAdapter.SimpleViewHolderModel<T, B>, B : ViewDataBinding>(
    list: List<T>,
    private var lifecycleOwner: LifecycleOwner? = null,
    private val layoutId: Int,
    offsetToLoadChunk: Int = ChunkListsConst.LiST_OFFSET_ITEM_TO_NEXT_PAGE
) : ChunkedListAdapter<SimpleListAdapter.SimpleViewHolder<T, VM, B>>(offsetToLoadChunk) {

    protected val list: MutableList<T> = list.toMutableList()

    protected abstract fun viewHolderModelBuilder(binding: B): VM

    abstract class SimpleViewHolderModel<T, B : ViewDataBinding>(val binding: B) {
        var position = -1
        abstract fun setData(data: T)
    }

    class SimpleViewHolder<T, VM : SimpleViewHolderModel<T, B>, B : ViewDataBinding>
    (binding: B, viewHolderModelBuilder: (B) -> VM) :
        RecyclerView.ViewHolder(binding.root) {

        private val viewModel = viewHolderModelBuilder(binding)

        init {
            binding.setVariable(BR.viewModel, viewModel)
        }

        fun setData(position: Int, data: T) {
            viewModel.position = position
            viewModel.setData(data)
        }
    }

    fun joinToLifeCycle(lifecycleOwner: LifecycleOwner) {
        this.lifecycleOwner = lifecycleOwner
    }

    fun getData(): List<T> {
        return list.toList()
    }

    fun clearData() {
        val removedSize = list.size
        list.clear()
        loadingItemsCount = 0
        queueItemRangeRemoved(0, removedSize)
    }

    fun removeItem(index: Int) {
        list.removeAt(index)
        if (loadingItemsCount > index) loadingItemsCount -= 1
        queueItemRemoved(index)
    }

    open fun setData(list: List<T>) {
        clearData()
        addData(list)
    }

    fun addData(list: List<T>, index: Int? = null) {
        if (list.isEmpty()) return
        index?.let { this.list.addAll(it, list) } ?: this.list.addAll(list)
        loadingNextChunk.value = false
        queueItemRangeInserted(index ?: this.list.size, list.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder<T, VM, B> {

        val binding = DataBindingUtil.inflate<B>(
            LayoutInflater.from(parent.context),
            layoutId,
            parent,
            false
        )

        return SimpleViewHolder(binding, ::viewHolderModelBuilder).apply {
            binding.lifecycleOwner = lifecycleOwner
        }
    }

    override fun onBindViewHolder(simpleHolder: SimpleViewHolder<T, VM, B>, position: Int) {
        super.onBindViewHolder(simpleHolder, position)
        simpleHolder.setData(position, list[position])
    }

    override fun getItemCount() = list.size
}
