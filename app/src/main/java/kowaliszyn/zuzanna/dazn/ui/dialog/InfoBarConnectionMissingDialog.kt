package kowaliszyn.zuzanna.dazn.ui.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import kowaliszyn.zuzanna.dazn.R
import kowaliszyn.zuzanna.dazn.ui.dialog.base.BaseDialog
import kowaliszyn.zuzanna.dazn.utils.extension.windowSize
import kowaliszyn.zuzanna.dazn.viewModel.dialog.InfoBarConnectionMissingDialogViewModel

@AndroidEntryPoint
class InfoBarConnectionMissingDialog private constructor(override val closeAction: (() -> Unit)? = null) : BaseDialog<InfoBarConnectionMissingDialogViewModel>() {

    companion object {
        fun newInstance(
            closeAction: (() -> Unit)? = null
        ) = InfoBarConnectionMissingDialog(closeAction)
    }

    override val viewModel by viewModels<InfoBarConnectionMissingDialogViewModel>()
    override val layoutId = R.layout.info_bar_connection_missing

    override fun onDrawWindow(window: Window) {
        window.apply {
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            attributes = attributes.apply {
                horizontalMargin = 0F
                verticalMargin = 0F
                activity?.windowSize?.let { size ->
                    height = size.height
                    width = size.width
                }
            }
        }
    }
}
