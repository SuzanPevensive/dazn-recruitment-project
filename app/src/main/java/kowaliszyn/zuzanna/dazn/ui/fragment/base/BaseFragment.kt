package kowaliszyn.zuzanna.dazn.ui.fragment.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kowaliszyn.zuzanna.dazn.manager.ErrorManager
import kowaliszyn.zuzanna.dazn.viewModel.fragment.base.BaseFragmentViewModel

abstract class BaseFragment<in T : Any, out VM : BaseFragmentViewModel<*, *>> : Fragment() {

    abstract val viewModel: VM

    protected abstract val errorManager: ErrorManager
    protected abstract val layoutId: Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel.init(null, layoutId, LayoutInflater.from(context))
        return viewModel.rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.joinToLifeCycle(viewLifecycleOwner)
        setObservers()
        errorManager.handleErrors(viewLifecycleOwner, viewModel::handleError)
    }

    abstract fun setObservers()
}
