package kowaliszyn.zuzanna.dazn.ui.dialog

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import kowaliszyn.zuzanna.dazn.R
import kowaliszyn.zuzanna.dazn.ui.dialog.base.BaseDialog
import kowaliszyn.zuzanna.dazn.viewModel.dialog.PlayerDialogViewModel

@AndroidEntryPoint
class PlayerDialog private constructor() : BaseDialog<PlayerDialogViewModel>() {

    companion object {
        fun newInstance() = PlayerDialog()
    }

    override val viewModel by viewModels<PlayerDialogViewModel>()
    override val layoutId = R.layout.dialog_player
    override val closeAction: (() -> Unit)? = null

    var videoUrl = ""

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        viewModel.releaseVideoPlayer()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isCancelable = true
        viewModel.initVideoPlayer(videoUrl)
    }
}
