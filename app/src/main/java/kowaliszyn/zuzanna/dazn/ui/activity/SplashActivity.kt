package kowaliszyn.zuzanna.dazn.ui.activity

import androidx.activity.viewModels
import dagger.hilt.android.AndroidEntryPoint
import kowaliszyn.zuzanna.dazn.R
import kowaliszyn.zuzanna.dazn.const.ActivityConst
import kowaliszyn.zuzanna.dazn.manager.ErrorManager
import kowaliszyn.zuzanna.dazn.ui.activity.base.BaseActivity
import kowaliszyn.zuzanna.dazn.utils.extension.goTo
import kowaliszyn.zuzanna.dazn.utils.extension.uiTimer
import kowaliszyn.zuzanna.dazn.viewModel.activity.SplashActivityViewModel
import javax.inject.Inject

@AndroidEntryPoint
class SplashActivity : BaseActivity<SplashActivityViewModel>() {

    override val viewModel by viewModels<SplashActivityViewModel>()

    @Inject
    override lateinit var errorManager: ErrorManager
    override val layoutId = R.layout.activity_splash

    override val checkConnectionState = false

    override fun setObservers() {
        viewModel.goToMainActivityEvent.observe(this) {
            uiTimer(ActivityConst.DELAY_SPLASH_SCREEN) {
                goTo<MainActivity>()
            }
        }
    }
}
