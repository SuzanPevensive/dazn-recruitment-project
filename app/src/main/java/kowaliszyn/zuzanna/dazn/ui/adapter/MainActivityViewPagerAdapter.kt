package kowaliszyn.zuzanna.dazn.ui.adapter

import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import kowaliszyn.zuzanna.dazn.enums.EventsFragmentMode
import kowaliszyn.zuzanna.dazn.ui.fragment.EventsFragment
import javax.inject.Inject

class MainActivityViewPagerAdapter @Inject constructor(
    activity: FragmentActivity
) : FragmentStateAdapter(activity) {

    override fun getItemCount() = 2

    override fun createFragment(position: Int) = EventsFragment.newInstance(
        EventsFragmentMode.values()[position]
    )
}
