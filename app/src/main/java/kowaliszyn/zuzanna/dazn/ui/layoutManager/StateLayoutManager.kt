package kowaliszyn.zuzanna.dazn.ui.layoutManager

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import kowaliszyn.zuzanna.dazn.utils.liveData.ScopedLiveData

class StateLayoutManager(context: Context) : LinearLayoutManager(context) {

    enum class State {
        ENABLED, DISABLED
    }

    val state = ScopedLiveData(State.ENABLED)

    fun enable() {
        state.value = State.ENABLED
    }

    fun disable() {
        state.value = State.DISABLED
    }

    override fun canScrollVertically(): Boolean {
        return state.value == State.ENABLED && super.canScrollVertically()
    }
}
