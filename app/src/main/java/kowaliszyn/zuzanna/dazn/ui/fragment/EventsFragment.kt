package kowaliszyn.zuzanna.dazn.ui.fragment

import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import kowaliszyn.zuzanna.dazn.R
import kowaliszyn.zuzanna.dazn.const.DialogConst
import kowaliszyn.zuzanna.dazn.const.FragmentConst.ARGUMENT_MODE
import kowaliszyn.zuzanna.dazn.enums.ConnectionState
import kowaliszyn.zuzanna.dazn.enums.EventsFragmentMode
import kowaliszyn.zuzanna.dazn.manager.ErrorManager
import kowaliszyn.zuzanna.dazn.ui.activity.base.BaseActivity
import kowaliszyn.zuzanna.dazn.ui.dialog.PlayerDialog
import kowaliszyn.zuzanna.dazn.ui.fragment.base.BaseFragment
import kowaliszyn.zuzanna.dazn.viewModel.fragment.EventsFragmentViewModel
import javax.inject.Inject

@AndroidEntryPoint
class EventsFragment private constructor() :
    BaseFragment<Boolean, EventsFragmentViewModel>() {

    companion object {
        fun newInstance(mode: EventsFragmentMode) =
            EventsFragment().apply {
                arguments = bundleOf(
                    ARGUMENT_MODE to mode
                )
            }
    }

    @Inject
    override lateinit var errorManager: ErrorManager
    override val layoutId = R.layout.fragment_events

    override val viewModel by viewModels<EventsFragmentViewModel>()

    private val playerDialog = PlayerDialog.newInstance()

    override fun setObservers() {
        activity?.let {
            if (it is BaseActivity<*>) {
                it.connectionState.observe(viewLifecycleOwner) { connectionState ->
                    if (connectionState == ConnectionState.CONNECTED) {
                        viewModel.refreshLayout(showWaiting = true)
                    }
                }
            }
        }
        viewModel.eventsListItemTapEvent.observe(viewLifecycleOwner) {
            playerDialog.videoUrl = it.videoUrl
            playerDialog.show(parentFragmentManager, DialogConst.TAG_DIALOG_VIDEO_PLAYER)
        }
        viewModel.adapter.loadingNextChunk.observe(viewLifecycleOwner) {
            if (it) viewModel.loadEventsNextChunk()
        }
    }
}
