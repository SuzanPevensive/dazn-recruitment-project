package kowaliszyn.zuzanna.dazn.enums

enum class EventsFragmentMode {
    PLAYER, SCHEDULE
}
