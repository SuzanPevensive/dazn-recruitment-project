package kowaliszyn.zuzanna.dazn.enums

enum class ConnectionState {
    CONNECTED, NOT_CONNECTED
}
