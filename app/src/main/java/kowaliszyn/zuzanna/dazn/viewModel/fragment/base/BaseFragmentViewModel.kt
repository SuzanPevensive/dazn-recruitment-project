package kowaliszyn.zuzanna.dazn.viewModel.fragment.base

import androidx.databinding.ViewDataBinding
import androidx.lifecycle.SavedStateHandle
import kowaliszyn.zuzanna.dazn.viewModel.base.BaseViewModel
import kowaliszyn.zuzanna.dazn.viewModel.repository.base.BaseRepository

abstract class BaseFragmentViewModel<B : ViewDataBinding, out R : BaseRepository> constructor(
    repository: R,
    savedStateHandle: SavedStateHandle
) : BaseViewModel<B, R>(repository, savedStateHandle)
