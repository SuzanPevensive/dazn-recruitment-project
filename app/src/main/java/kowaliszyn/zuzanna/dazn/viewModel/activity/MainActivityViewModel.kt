package kowaliszyn.zuzanna.dazn.viewModel.activity

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.SavedStateHandle
import androidx.viewpager2.widget.ViewPager2
import dagger.hilt.android.lifecycle.HiltViewModel
import kowaliszyn.zuzanna.dazn.R
import kowaliszyn.zuzanna.dazn.databinding.ActivityMainBinding
import kowaliszyn.zuzanna.dazn.enums.EventsFragmentMode
import kowaliszyn.zuzanna.dazn.ui.adapter.MainActivityViewPagerAdapter
import kowaliszyn.zuzanna.dazn.viewModel.base.BaseViewModel
import kowaliszyn.zuzanna.dazn.viewModel.repository.activity.MainActivityRepository
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor(
    repository: MainActivityRepository,
    savedStateHandle: SavedStateHandle,
) : BaseViewModel<ActivityMainBinding, MainActivityRepository>(
    repository,
    savedStateHandle
) {

    private val onPageChangeCallback = object : ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            binding?.bottomNavigation?.selectedItemId =
                if (position == 0) R.id.menu_item_events else R.id.menu_item_schedule
        }
    }

    override fun init(
        lifecycleOwner: LifecycleOwner?,
        layoutId: Int,
        inflater: LayoutInflater,
        container: ViewGroup?,
    ) {
        super.init(lifecycleOwner, layoutId, inflater, container)
        binding?.bottomNavigation?.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.menu_item_events -> {
                    goToEventsFragment(); true
                }
                R.id.menu_item_schedule -> {
                    goToSchedulerFragment(); true
                }
                else -> false
            }
        }
    }

    fun initViewPager(adapter: MainActivityViewPagerAdapter) {
        binding?.mainViewpager?.adapter = adapter
        binding?.mainViewpager?.registerOnPageChangeCallback(onPageChangeCallback)
    }

    private fun goToEventsFragment() {
        binding?.mainViewpager?.currentItem = EventsFragmentMode.PLAYER.ordinal
    }
    private fun goToSchedulerFragment() {
        binding?.mainViewpager?.currentItem = EventsFragmentMode.SCHEDULE.ordinal
    }
}
