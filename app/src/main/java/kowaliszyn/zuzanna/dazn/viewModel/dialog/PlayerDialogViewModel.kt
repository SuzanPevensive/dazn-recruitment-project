package kowaliszyn.zuzanna.dazn.viewModel.dialog

import android.net.Uri
import androidx.lifecycle.SavedStateHandle
import com.google.android.exoplayer2.C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import dagger.hilt.android.lifecycle.HiltViewModel
import kowaliszyn.zuzanna.dazn.databinding.DialogPlayerBinding
import kowaliszyn.zuzanna.dazn.utils.event.impl.SingleClickEvent
import kowaliszyn.zuzanna.dazn.viewModel.dialog.base.BaseDialogViewModel
import kowaliszyn.zuzanna.dazn.viewModel.repository.dialog.PlayerDialogRepository
import javax.inject.Inject

@HiltViewModel
class PlayerDialogViewModel @Inject constructor(
    repositoryPlayerDialog: PlayerDialogRepository,
    savedStateHandle: SavedStateHandle
) : BaseDialogViewModel<DialogPlayerBinding, PlayerDialogRepository>(
    repositoryPlayerDialog,
    savedStateHandle
) {

    private var player: SimpleExoPlayer? = null

    fun initVideoPlayer(videoUrl: String) {
        SimpleExoPlayer.Builder(context).build().apply {

            videoScalingMode = VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING
            player = this

            binding?.apply {
                playerDialogVideoPlayer.player = player
                playerDialogVideoContainer.setAspectRatio(4f / 3f)
            }

            setMediaItem(MediaItem.fromUri(Uri.parse(videoUrl)))
            prepare()
            play()
        }
    }

    fun releaseVideoPlayer() {
        player?.stop()
        player?.release()
    }

    fun closePlayer(event: SingleClickEvent) {
        releaseVideoPlayer()
        closeDialogEvent.run()
        event.done()
    }
}
