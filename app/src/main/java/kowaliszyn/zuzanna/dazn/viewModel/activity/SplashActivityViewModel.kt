package kowaliszyn.zuzanna.dazn.viewModel.activity

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.SavedStateHandle
import dagger.hilt.android.lifecycle.HiltViewModel
import kowaliszyn.zuzanna.dazn.databinding.ActivitySplashBinding
import kowaliszyn.zuzanna.dazn.utils.extension.global.onScope
import kowaliszyn.zuzanna.dazn.utils.liveData.SingleEventLiveData
import kowaliszyn.zuzanna.dazn.viewModel.base.BaseViewModel
import kowaliszyn.zuzanna.dazn.viewModel.repository.activity.SplashActivityRepository
import javax.inject.Inject

@HiltViewModel
class SplashActivityViewModel @Inject constructor(
    repository: SplashActivityRepository,
    savedStateHandle: SavedStateHandle
) : BaseViewModel<ActivitySplashBinding, SplashActivityRepository>(repository, savedStateHandle) {

    val goToMainActivityEvent = SingleEventLiveData()

    override fun init(
        lifecycleOwner: LifecycleOwner?,
        layoutId: Int,
        inflater: LayoutInflater,
        container: ViewGroup?
    ) {
        super.init(lifecycleOwner, layoutId, inflater, container)
        onScope {
            repository.clearAllData()
        }
        goToMainActivityEvent.run()
    }
}
