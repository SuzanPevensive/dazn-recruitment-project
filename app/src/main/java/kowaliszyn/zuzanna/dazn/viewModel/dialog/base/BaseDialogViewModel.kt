package kowaliszyn.zuzanna.dazn.viewModel.dialog.base

import androidx.databinding.ViewDataBinding
import androidx.lifecycle.SavedStateHandle
import kowaliszyn.zuzanna.dazn.utils.liveData.EventLiveData
import kowaliszyn.zuzanna.dazn.viewModel.base.BaseViewModel
import kowaliszyn.zuzanna.dazn.viewModel.repository.base.BaseRepository

abstract class BaseDialogViewModel<B : ViewDataBinding, out R : BaseRepository> constructor(
    repository: R,
    savedStateHandle: SavedStateHandle
) : BaseViewModel<B, R>(repository, savedStateHandle) {

    val closeDialogEvent = EventLiveData()
}
