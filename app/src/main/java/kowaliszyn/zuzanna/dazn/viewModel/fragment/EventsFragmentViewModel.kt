package kowaliszyn.zuzanna.dazn.viewModel.fragment

import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.SavedStateHandle
import dagger.hilt.android.lifecycle.HiltViewModel
import kowaliszyn.zuzanna.dazn.const.ChunkListsConst
import kowaliszyn.zuzanna.dazn.const.FragmentConst
import kowaliszyn.zuzanna.dazn.databinding.FragmentEventsBinding
import kowaliszyn.zuzanna.dazn.enums.EventsFragmentMode
import kowaliszyn.zuzanna.dazn.model.data.EventData
import kowaliszyn.zuzanna.dazn.ui.adapter.EventsListAdapter
import kowaliszyn.zuzanna.dazn.ui.layoutManager.StateLayoutManager
import kowaliszyn.zuzanna.dazn.utils.extension.global.onMain
import kowaliszyn.zuzanna.dazn.utils.extension.global.onScope
import kowaliszyn.zuzanna.dazn.utils.liveData.ScopedLiveData
import kowaliszyn.zuzanna.dazn.viewModel.fragment.base.BaseFragmentViewModel
import kowaliszyn.zuzanna.dazn.viewModel.repository.fragment.EventsFragmentRepository
import java.util.*
import javax.inject.Inject
import kotlin.concurrent.timerTask

@HiltViewModel
class EventsFragmentViewModel @Inject constructor(
    repository: EventsFragmentRepository,
    savedStateHandle: SavedStateHandle,
) : BaseFragmentViewModel<FragmentEventsBinding, EventsFragmentRepository>(
    repository,
    savedStateHandle
) {

    private var mode = EventsFragmentMode.PLAYER

    val adapter = EventsListAdapter(listOf()) { eventId ->
        if (mode == EventsFragmentMode.PLAYER) {
            onScope {
                repository.getEvent(eventId)?.let { eventsListItemTapEvent.value = it }
            }
        }
    }
    private val layoutManager = StateLayoutManager(context)

    private var listEventsSwipeRefreshTimestamp = 0L
    val isWaiting = ScopedLiveData(false)
    val eventsListItemTapEvent = ScopedLiveData<EventData>()

    private val handler: Handler = Handler(Looper.getMainLooper())
    private val reloadSchedulerRunnable = object : Runnable {

        fun postDelayed() {
            handler.postDelayed(this, FragmentConst.RELOAD_SCHEDULER_TIMER_DELAY)
        }

        override fun run() {
            onScope {
                repository.getSchedule()?.let {
                    adapter.setSchedule(it)
                }
            }
            postDelayed()
        }
    }

    override fun init(
        lifecycleOwner: LifecycleOwner?,
        layoutId: Int,
        inflater: LayoutInflater,
        container: ViewGroup?,
    ) {
        super.init(lifecycleOwner, layoutId, inflater, container)
        savedStateHandle.apply {
            get<EventsFragmentMode>(FragmentConst.ARGUMENT_MODE)?.let {
                mode = it
            }
        }
        binding?.listEventsSwipeRefresh?.setOnRefreshListener(::refreshLayout)
        if (mode == EventsFragmentMode.SCHEDULE) {
            adapter.chunked = false
            reloadSchedulerRunnable.postDelayed()
            loadEventsNextChunk(true)
        } else {
            onScope {
                repository.updateEvents()
                loadEventsNextChunk(true)
            }
        }
    }

    override fun joinToLifeCycle(lifecycleOwner: LifecycleOwner) {
        super.joinToLifeCycle(lifecycleOwner)
        adapter.joinToLifeCycle(lifecycleOwner)
    }

    fun refreshLayout(showWaiting: Boolean = false) {
        repository.resetChunkCounter()
        loadEventsNextChunk(refreshList = true, showWaiting = showWaiting)
        binding?.listEventsSwipeRefresh?.isRefreshing = false
    }

    fun loadEventsNextChunk(
        initialLoad: Boolean = false,
        refreshList: Boolean = false,
        showWaiting: Boolean = true
    ) {
        listEventsSwipeRefreshTimestamp = System.currentTimeMillis()
        isWaiting.value = showWaiting
        layoutManager.disable()
        onScope {
            if (mode == EventsFragmentMode.SCHEDULE) {
                repository.getSchedule()?.let { events ->
                    onMain {
                        adapter.setSchedule(events)
                    }
                }
            } else {
                if (refreshList) repository.updateEvents()
                repository.getEventsNextChunk().let { events ->
                    onMain {
                        if (refreshList) adapter.setData(events) else adapter.addData(events)
                    }
                }
            }
            onMain {
                if (initialLoad || adapter.maxItemCount == 0) {
                    adapter.maxItemCount = repository.getEventsListSize()
                }
                if (initialLoad) {
                    binding?.listEvents?.let { recyclerView ->
                        recyclerView.adapter = adapter
                        recyclerView.layoutManager = layoutManager
                    }
                }
                layoutManager.enable()
            }
            val timestampDiff = ChunkListsConst.LIST_CHUNK_LOAD_MIN_DELAY -
                (System.currentTimeMillis() - listEventsSwipeRefreshTimestamp)
            if (timestampDiff > 0) {
                Timer(false).schedule(
                    timerTask {
                        isWaiting.value = false
                    },
                    timestampDiff
                )
            } else isWaiting.value = false
        }
    }
}
