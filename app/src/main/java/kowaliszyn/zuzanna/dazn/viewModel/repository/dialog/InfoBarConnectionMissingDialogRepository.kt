package kowaliszyn.zuzanna.dazn.viewModel.repository.dialog

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import kowaliszyn.zuzanna.dazn.converter.DataConverter
import kowaliszyn.zuzanna.dazn.manager.DataManager
import kowaliszyn.zuzanna.dazn.manager.NetworkManager
import kowaliszyn.zuzanna.dazn.viewModel.repository.base.BaseRepository
import javax.inject.Inject

class InfoBarConnectionMissingDialogRepository @Inject constructor(
    @ApplicationContext context: Context,
    dataConverter: DataConverter,
    dataManager: DataManager,
    networkManager: NetworkManager
) : BaseRepository(context, dataConverter, dataManager, networkManager)
