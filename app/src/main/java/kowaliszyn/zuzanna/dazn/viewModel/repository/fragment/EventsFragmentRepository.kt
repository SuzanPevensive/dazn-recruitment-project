package kowaliszyn.zuzanna.dazn.viewModel.repository.fragment

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import kowaliszyn.zuzanna.dazn.converter.DataConverter
import kowaliszyn.zuzanna.dazn.manager.DataManager
import kowaliszyn.zuzanna.dazn.manager.NetworkManager
import kowaliszyn.zuzanna.dazn.model.data.EventData
import kowaliszyn.zuzanna.dazn.viewModel.repository.base.BaseRepository
import javax.inject.Inject

class EventsFragmentRepository @Inject constructor(
    @ApplicationContext context: Context,
    dataConverter: DataConverter,
    dataManager: DataManager,
    networkManager: NetworkManager,
) : BaseRepository(context, dataConverter, dataManager, networkManager) {

    private var chunk = 0

    fun resetChunkCounter() {
        chunk = 0
    }

    suspend fun getSchedule() = networkManager.getSchedule()?.let {
        dataConverter.convertEventsResponse(it)
    }?.sortedBy { it.date.time }

    suspend fun getEventsListSize() = dataManager.getAllEvents().size
    suspend fun getEvent(eventId: Int) = dataManager.getEvent(eventId)

    suspend fun getEventsNextChunk(): List<EventData> {
        return dataManager.getEvents(chunk).also {
            chunk += 1
        }
    }

    suspend fun updateEvents() =
        networkManager.getEvents()?.let { eventsList ->
            dataManager.clearAllData()
            dataManager.addEvents(dataConverter.convertEventsResponse(eventsList))
        }
}
