package kowaliszyn.zuzanna.dazn.viewModel.dialog

import androidx.lifecycle.SavedStateHandle
import dagger.hilt.android.lifecycle.HiltViewModel
import kowaliszyn.zuzanna.dazn.databinding.InfoBarConnectionMissingBinding
import kowaliszyn.zuzanna.dazn.utils.event.impl.SingleClickEvent
import kowaliszyn.zuzanna.dazn.viewModel.dialog.base.BaseDialogViewModel
import kowaliszyn.zuzanna.dazn.viewModel.repository.dialog.InfoBarConnectionMissingDialogRepository
import javax.inject.Inject

@HiltViewModel
class InfoBarConnectionMissingDialogViewModel @Inject constructor(
    repositoryConnectionMissing: InfoBarConnectionMissingDialogRepository,
    savedStateHandle: SavedStateHandle
) : BaseDialogViewModel<InfoBarConnectionMissingBinding, InfoBarConnectionMissingDialogRepository>(repositoryConnectionMissing, savedStateHandle) {

    fun tryReconnect(event: SingleClickEvent) {
        closeDialogEvent.run()
        event.done()
    }
}
