package kowaliszyn.zuzanna.dazn.viewModel.repository.base

import android.content.Context
import kowaliszyn.zuzanna.dazn.converter.DataConverter
import kowaliszyn.zuzanna.dazn.manager.DataManager
import kowaliszyn.zuzanna.dazn.manager.NetworkManager

abstract class BaseRepository constructor(
    val context: Context,
    val dataConverter: DataConverter,
    val dataManager: DataManager,
    val networkManager: NetworkManager
)
