package kowaliszyn.zuzanna.dazn.utils.dataBindingAdapter

import android.view.View
import androidx.databinding.BindingAdapter

object DataBindingAdapterVisibility {

    @JvmStatic
    @BindingAdapter("isVisibility")
    fun setVisibility(view: View, visibility: Boolean) {
        view.visibility = when {
            visibility -> View.VISIBLE
            invisibleInsteadGoneMap[view] == true -> View.INVISIBLE
            else -> View.GONE
        }
    }

    private val invisibleInsteadGoneMap = mutableMapOf<View, Boolean>()
    @JvmStatic
    @BindingAdapter("invisibleInsteadGone")
    fun invisibleInsteadGone(view: View, isTrue: Boolean) {
        invisibleInsteadGoneMap[view] = isTrue
        if (view.visibility != View.VISIBLE) setVisibility(view, false)
    }
}
