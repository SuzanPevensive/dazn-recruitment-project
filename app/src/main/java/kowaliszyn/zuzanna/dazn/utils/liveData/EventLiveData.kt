package kowaliszyn.zuzanna.dazn.utils.liveData

class EventLiveData : ClearableLiveData<Unit?>() {

    fun run() {
        value = Unit
    }
}
