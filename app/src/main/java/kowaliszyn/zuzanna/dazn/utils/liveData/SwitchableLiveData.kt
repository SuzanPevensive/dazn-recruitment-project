package kowaliszyn.zuzanna.dazn.utils.liveData

class SwitchableLiveData : UniqueLiveData<Boolean> {

    constructor(value: Boolean) : super(value)
    constructor() : super()

    fun switch() {
        value = !(value ?: false)
    }
}
