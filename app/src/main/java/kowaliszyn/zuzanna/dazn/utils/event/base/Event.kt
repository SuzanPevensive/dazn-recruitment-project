package kowaliszyn.zuzanna.dazn.utils.event.base

interface Event {
    fun done()
}
