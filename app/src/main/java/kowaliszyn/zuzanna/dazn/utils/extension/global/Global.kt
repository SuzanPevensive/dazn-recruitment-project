package kowaliszyn.zuzanna.dazn.utils.extension.global

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

fun onScope(action: suspend CoroutineScope.() -> Unit) =
    CoroutineScope(Dispatchers.Default).launch(block = action)
suspend fun <R> withScope(action: suspend CoroutineScope.() -> R) =
    withContext(Dispatchers.Default, block = action)
fun onMainScope(action: suspend CoroutineScope.() -> Unit) =
    CoroutineScope(Dispatchers.Main).launch(block = action)
suspend fun onMain(action: suspend CoroutineScope.() -> Unit) =
    withContext(Dispatchers.Main, action)
