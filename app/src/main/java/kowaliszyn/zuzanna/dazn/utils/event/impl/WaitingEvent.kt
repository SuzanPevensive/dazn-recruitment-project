package kowaliszyn.zuzanna.dazn.utils.event.impl

import android.view.View
import android.view.ViewGroup
import androidx.annotation.VisibleForTesting
import androidx.core.view.children
import kowaliszyn.zuzanna.dazn.utils.event.base.Event
import kowaliszyn.zuzanna.dazn.utils.extension.disabled
import kowaliszyn.zuzanna.dazn.utils.extension.enabled
import kowaliszyn.zuzanna.dazn.utils.view.WaitIconView

open class WaitingEvent(vararg views: View) : Event {

    @VisibleForTesting
    var isDone = false
    private val views = views.toList()

    private fun getWaitIcon(view: View) =
        (view.parent as ViewGroup).children.firstOrNull { it is WaitIconView }

    init {
        this.views.forEach { view ->
            view.disabled()
            getWaitIcon(view)?.visibility = View.VISIBLE
        }
    }

    override fun done() {
        if (isDone) return
        views.forEach { view ->
            view.enabled()
            getWaitIcon(view)?.visibility = View.GONE
        }
        isDone = true
    }
}
