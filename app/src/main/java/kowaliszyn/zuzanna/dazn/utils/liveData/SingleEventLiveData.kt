package kowaliszyn.zuzanna.dazn.utils.liveData

class SingleEventLiveData : UniqueLiveData<Unit>() {

    fun run() {
        value = Unit
    }
}
