package kowaliszyn.zuzanna.dazn.utils.event.impl

import android.view.View

class SingleClickEvent(var view: View) : WaitingEvent(view)
