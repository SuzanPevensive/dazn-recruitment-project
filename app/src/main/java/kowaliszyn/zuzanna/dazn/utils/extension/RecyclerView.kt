package kowaliszyn.zuzanna.dazn.utils.extension

import androidx.recyclerview.widget.RecyclerView
import kowaliszyn.zuzanna.dazn.ui.layoutManager.StateLayoutManager

val RecyclerView.stateLayoutManager get() = layoutManager as StateLayoutManager
