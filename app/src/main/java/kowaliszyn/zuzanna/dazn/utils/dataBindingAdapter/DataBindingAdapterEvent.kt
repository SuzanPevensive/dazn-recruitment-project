package kowaliszyn.zuzanna.dazn.utils.dataBindingAdapter

import android.view.View
import androidx.databinding.BindingAdapter
import kowaliszyn.zuzanna.dazn.utils.event.impl.SingleClickEvent
import kowaliszyn.zuzanna.dazn.utils.extension.hideKeyboard

object DataBindingAdapterEvent {

    interface OnSingleClickListener {
        fun onSingleClick(event: SingleClickEvent)
    }

    @JvmStatic
    @BindingAdapter("onSingleClick")
    fun setOnSingleClickListener(view: View, singleClickListener: OnSingleClickListener) {
        view.apply {
            isFocusable = false
            setOnClickListener {
                rootView.findFocus()?.hideKeyboard()
                isClickable = false
                singleClickListener.onSingleClick(SingleClickEvent(this))
            }
        }
    }
}
